package com.js.housekeepingapi.repository;

import com.js.housekeepingapi.entity.Housekeeping;
import org.springframework.data.jpa.repository.JpaRepository;

public interface HouseRepository extends JpaRepository <Housekeeping, Long> {
}
