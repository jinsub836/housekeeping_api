package com.js.housekeepingapi.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum AmountType {

    FOOD("식비",false,false),
    BUS("교통비",true,false),
    CLOTH("의류비",false,false),
    MOBILE("통신비",true,false),
    ENTERTAIN("오락",false,false),
    EDUCATION("교육비",false,false),
    APPLIANCES ("가전 제품",false,false),
    SALARY("월급",true,true),
    TRAINING("훈련비",true,true);

    private final String name;
    private final Boolean fixedExpenditure;
    private final Boolean income;
}
