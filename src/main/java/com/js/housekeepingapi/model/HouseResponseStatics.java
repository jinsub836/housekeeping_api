package com.js.housekeepingapi.model;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class HouseResponseStatics {
    private Integer totalAmount;
    private Integer averageAmount;
}
