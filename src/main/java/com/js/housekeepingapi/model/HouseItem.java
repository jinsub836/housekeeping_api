package com.js.housekeepingapi.model;

import com.js.housekeepingapi.enums.AmountType;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class HouseItem {
    private Long id;
    private LocalDate dateMake;
    private Integer amount;
    private String amountType;
    private Boolean fixedExpenditure;
    private Boolean income;
}
