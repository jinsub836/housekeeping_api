package com.js.housekeepingapi.model;

import com.js.housekeepingapi.enums.AmountType;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class HouseRequest {

    private Integer amount;

    @Enumerated(value = EnumType.STRING)

    private AmountType amountType;

    private String etcMemo;
}
