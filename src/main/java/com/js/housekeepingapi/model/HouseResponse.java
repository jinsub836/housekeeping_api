package com.js.housekeepingapi.model;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class HouseResponse {

    private Long id;
    private LocalDate dateMake;
    private Integer amount;
    private String name;
    private String fixedExpenditureName;
    private String incomeName;
    private String etcMemo;

}
