package com.js.housekeepingapi.controller;

import com.js.housekeepingapi.model.HouseItem;
import com.js.housekeepingapi.model.HouseResponseStatics;
import com.js.housekeepingapi.model.HouseRequest;
import com.js.housekeepingapi.model.HouseResponse;
import com.js.housekeepingapi.service.HouseService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@RequiredArgsConstructor
@RequestMapping("/house")
public class HouseController {
    private final HouseService houseService;

    @PostMapping("/money")
    public String setHouseService(@RequestBody HouseRequest request){
        houseService.setHouse(request);
        return "기록 되었습니다.";
    }

    @GetMapping("/all")
    public List<HouseItem> getHouses(){
     return houseService.getHouses();
    }

    @GetMapping("/detail/{id}")
    public HouseResponse getHouse(@PathVariable long id){
        return houseService.getHouse(id);
    }

    @GetMapping("/statics")
    public HouseResponseStatics getStatics(){
        return houseService.getStatics();
    }
}

