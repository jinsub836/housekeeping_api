package com.js.housekeepingapi.service;

import com.js.housekeepingapi.entity.Housekeeping;
import com.js.housekeepingapi.model.HouseItem;
import com.js.housekeepingapi.model.HouseResponseStatics;
import com.js.housekeepingapi.model.HouseRequest;
import com.js.housekeepingapi.model.HouseResponse;
import com.js.housekeepingapi.repository.HouseRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class HouseService {
    private final HouseRepository houseRepository;

    public void setHouse(HouseRequest request){
        Housekeeping addDate = new Housekeeping();
        addDate.setDateMake(LocalDate.now());
        addDate.setAmount(request.getAmount());
        addDate.setAmountType(request.getAmountType());
        addDate.setEtcMemo(request.getEtcMemo());

        houseRepository.save(addDate);
    }

    public List<HouseItem> getHouses(){
        List<Housekeeping> originlist = houseRepository.findAll();
        List<HouseItem> result = new LinkedList<>();

        for (Housekeeping housekeeping : originlist ){
            HouseItem addItem = new HouseItem();
            addItem.setId(housekeeping.getId());
            addItem.setDateMake(housekeeping.getDateMake());
            addItem.setAmount(housekeeping.getAmount());
            addItem.setAmountType(housekeeping.getAmountType().getName());
            addItem.setFixedExpenditure(housekeeping.getAmountType().getFixedExpenditure());
            addItem.setIncome(housekeeping.getAmountType().getIncome());

            result.add(addItem);
        }
        return result;

    }

    public HouseResponse getHouse(long id){
        Housekeeping originData = houseRepository.findById(id).orElseThrow();

        HouseResponse response = new HouseResponse();
        response.setId(originData.getId());
        response.setDateMake(originData.getDateMake());
        response.setAmount(originData.getAmount());
        response.setName(originData.getAmountType().getName());
        response.setFixedExpenditureName(originData.getAmountType().getFixedExpenditure() ? "고정 지출":"일반 지출");
        response.setIncomeName(originData.getAmountType().getIncome() ? "수입" : "지출");
        response.setEtcMemo(originData.getEtcMemo());

        return response;
    }

    public HouseResponseStatics getStatics(){
        HouseResponseStatics houseResponseStatics = new HouseResponseStatics();
        List<Housekeeping> originlist = houseRepository.findAll();

        int totalAmount = 0;

        for (Housekeeping housekeeping : originlist ){

            if (housekeeping.getAmountType().getIncome().equals(false))
            { totalAmount += housekeeping.getAmount();}

            int averageAmount= totalAmount / originlist.size();

            houseResponseStatics.setTotalAmount(totalAmount);
            houseResponseStatics.setAverageAmount(averageAmount);
        } return houseResponseStatics;
    }
}
